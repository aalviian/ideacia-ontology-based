from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader
from django.shortcuts import render_to_response
from django.middleware import csrf
import numpy as np
import requests
import sys
import pprint
import os
from rdflib import Graph
import re

# Create your views here.
def index(request):
    return render(request, 'index.html') 

def about_us(request):
    return render(request, 'about_us.html') 

def algorithm(request):
    return render(request, 'algorithm.html') 

def benchmark(request):
    return render(request, 'benchmark.html') 

def team(request):
    return render(request, 'team.html') 

def querybm25(request):
    def get_disease_kueri_bebas(query):
        url = 'http://localhost:8983/solr/onto-bm25/select?debugQuery=on&indent=on&q='+query+'&rows=1000&wt=json'
        try:
            result = requests.get(url)
            result = result.json()
            if result != None:
                result_response = None
                if "response" in result: 
                    result_response = result["response"] 
                    count = len(result_response['docs'])

            name = []
            english_name = []
            description = []
            pathogen = []
            location = []
            spreading = []
            symptom = []
            treatment = []
            for result in result_response['docs']:
                if('gejala' in result['id']): continue
                elif('pengendalian' in result['id']): continue
                else:
                    if('caused_by' in result):
                        name.append(result['name'][0].strip())
                        english_name.append(result['english_name'][0].strip())
                        description.append(result['description'][0].strip())
                        pathogen.append([])
                        for row in result['caused_by']:
                            pathogen[-1].append(row.strip())
                        location.append(result['location_in'][0].strip())
                        spreading.append(result['spreading'][0].strip())
                        symptom.append(result['has_symptoms'][0].strip())
                        treatment.append(result['has_treatment'][0].strip())

                    else: continue
            # if(len(name) == 0):
            #   print("\nOops, pencarian tidak ditemukan\n")
            #   sys.exit()

            return url, query, name, english_name, description, pathogen, location, spreading, symptom, treatment
            
        except requests.exceptions.RequestException as e:
            print(e)
            sys.exit(1)

    def get_name_bebas(name):
        nama = []
        for r in name:
            nama.append(r)
        return nama

    def get_english_name_bebas(english_name):
        nama_inggris = []
        for r in english_name:
            nama_inggris.append(r)
        return nama_inggris

    def get_description(description):
        deskripsi = []
        for r in description:
            deskripsi.append(r)
        return deskripsi

    def get_pathogen(pathogen):
        patogen = []
        for r in pathogen:
            patogen.append(r)
        return patogen

    def get_location(location):
        lokasi = []
        for r in location:
            lokasi.append(r)
        return lokasi 

    def get_spreading(spreading):
        penyebaran = []
        for r in spreading:
            penyebaran.append(r)
        return penyebaran 

    def get_symptom(symptom):
        module_dir = os.path.dirname(__file__)
        acacia = Graph().parse(os.path.join(module_dir, 'acacia.owl'))
        gejala = []
        for symptom in symptom:
            url = 'http://localhost:8983/solr/onto-bm25/select?debugQuery=on&indent=on&q=id:*'+symptom+'*&wt=json'
            try:
                result = requests.get(url)
                result = result.json()
                if result != None:
                    result_response = None
                    if "response" in result: 
                        result_response = result["response"] 
                        count = len(result_response['docs'])

                for result in result_response['docs']:
                    if('gejala' in result['id']):
                        gejala.append(result['symptom'][0])
                    else: continue
            
            except requests.exceptions.RequestException as e:
                break
        return gejala
                    
    def get_treatment(treatment):
        module_dir = os.path.dirname(__file__)
        acacia = Graph().parse(os.path.join(module_dir, 'acacia.owl'))
        pengendalian = []
        for treatment in treatment:
            url = 'http://localhost:8983/solr/onto-bm25/select?debugQuery=on&indent=on&q=id:*'+treatment+'*&wt=json'
            try:
                result = requests.get(url)
                result = result.json()
                if result != None:
                    result_response = None
                    if "response" in result: 
                        result_response = result["response"] 
                        count = len(result_response['docs'])

                for result in result_response['docs']:
                    if('pengendalian' in result['id']):
                        pengendalian.append(result['treatment'][0])
                    else: continue
            
            except requests.exceptions.RequestException as e:
                break
        return pengendalian

    query = request.POST.get('query', '')
    csrf_token = csrf.get_token(request)
    url, query, name, english_name, description, pathogen, location, spreading, symptom, treatment = get_disease_kueri_bebas(query)

    name = get_name_bebas(name)
    english_name = get_english_name_bebas(english_name)
    description = get_description(description)
    pathogen = get_pathogen(pathogen)
    location = get_location(location)
    spreading = get_spreading(spreading)
    symptom = get_symptom(symptom)
    treatment = get_treatment(treatment)

    zipped = zip(name, english_name, description, pathogen, location, spreading, symptom, treatment)
    data = list(zipped)
    return render_to_response('index.html',{'query': query,'url': url, 'patogen': pathogen, 'data': data, 'csrf_token' : csrf_token })

def querytfidf(request):
    def get_disease_kueri_bebas(query):
        url = 'http://localhost:8983/solr/onto-default/select?debugQuery=on&indent=on&q='+query+'&rows=1000&wt=json'
        try:
            result = requests.get(url)
            result = result.json()
            if result != None:
                result_response = None
                if "response" in result: 
                    result_response = result["response"] 
                    count = len(result_response['docs'])

            name = []
            english_name = []
            description = []
            pathogen = []
            location = []
            spreading = []
            symptom = []
            treatment = []
            for result in result_response['docs']:
                if('gejala' in result['id']): continue
                elif('pengendalian' in result['id']): continue
                else:
                    if('caused_by' in result):
                        name.append(result['name'][0].strip())
                        english_name.append(result['english_name'][0].strip())
                        description.append(result['description'][0].strip())
                        pathogen.append([])
                        for row in result['caused_by']:
                            pathogen[-1].append(row.strip())
                        location.append(result['location_in'][0].strip())
                        spreading.append(result['spreading'][0].strip())
                        symptom.append(result['has_symptoms'][0].strip())
                        treatment.append(result['has_treatment'][0].strip())

                    else: continue
            # if(len(name) == 0):
            #   print("\nOops, pencarian tidak ditemukan\n")
            #   sys.exit()

            return url, query, name, english_name, description, pathogen, location, spreading, symptom, treatment
            
        except requests.exceptions.RequestException as e:
            print(e)
            sys.exit(1)

    def get_name_bebas(name):
        nama = []
        for r in name:
            nama.append(r)
        return nama

    def get_english_name_bebas(english_name):
        nama_inggris = []
        for r in english_name:
            nama_inggris.append(r)
        return nama_inggris

    def get_description(description):
        deskripsi = []
        for r in description:
            deskripsi.append(r)
        return deskripsi

    def get_pathogen(pathogen):
        patogen = []
        for r in pathogen:
            patogen.append(r)
        return patogen

    def get_location(location):
        lokasi = []
        for r in location:
            lokasi.append(r)
        return lokasi 

    def get_spreading(spreading):
        penyebaran = []
        for r in spreading:
            penyebaran.append(r)
        return penyebaran 

    def get_symptom(symptom):
        module_dir = os.path.dirname(__file__)
        acacia = Graph().parse(os.path.join(module_dir, 'acacia.owl'))
        gejala = []
        for symptom in symptom:
            url = 'http://localhost:8983/solr/onto-default/select?debugQuery=on&indent=on&q=id:*'+symptom+'*&wt=json'
            try:
                result = requests.get(url)
                result = result.json()
                if result != None:
                    result_response = None
                    if "response" in result: 
                        result_response = result["response"] 
                        count = len(result_response['docs'])

                for result in result_response['docs']:
                    if('gejala' in result['id']):
                        gejala.append(result['symptom'][0])
                    else: continue
            
            except requests.exceptions.RequestException as e:
                break
        return gejala
                    
    def get_treatment(treatment):
        module_dir = os.path.dirname(__file__)
        acacia = Graph().parse(os.path.join(module_dir, 'acacia.owl'))
        pengendalian = []
        for treatment in treatment:
            url = 'http://localhost:8983/solr/onto-default/select?debugQuery=on&indent=on&q=id:*'+treatment+'*&wt=json'
            try:
                result = requests.get(url)
                result = result.json()
                if result != None:
                    result_response = None
                    if "response" in result: 
                        result_response = result["response"] 
                        count = len(result_response['docs'])

                for result in result_response['docs']:
                    if('pengendalian' in result['id']):
                        pengendalian.append(result['treatment'][0])
                    else: continue
            
            except requests.exceptions.RequestException as e:
                break
        return pengendalian

    query = request.POST.get('query', '')
    csrf_token = csrf.get_token(request)
    url, query, name, english_name, description, pathogen, location, spreading, symptom, treatment = get_disease_kueri_bebas(query)

    name = get_name_bebas(name)
    english_name = get_english_name_bebas(english_name)
    description = get_description(description)
    pathogen = get_pathogen(pathogen)
    location = get_location(location)
    spreading = get_spreading(spreading)
    symptom = get_symptom(symptom)
    treatment = get_treatment(treatment)

    zipped = zip(name, english_name, description, pathogen, location, spreading, symptom, treatment)
    data = list(zipped)
    return render_to_response('index.html',{'query2': query,'url2': url, 'patogen2': pathogen, 'data2': data, 'csrf_token' : csrf_token })

def query_sparql(request):
    def get_disease_kueri_form(inang, patogen, area, warna, bentuk, kondisi,cuaca):
        tipe = patogen+"_"+inang
        module_dir = os.path.dirname(__file__)
        file = os.path.join(module_dir, 'acacia.owl')
        acacia = Graph().parse(file)

        if(not(warna) and not(bentuk) and not(kondisi) and not(cuaca)): #jumlah_atribut = 9
            qres = acacia.query( 
                """ 
                PREFIX tst: <http://www.semanticweb.org/aalviian/ontologies/2017/1/untitled-ontology-10#>
                SELECT ?disease ?name ?english_name ?desc ?type ?area ?spreading ?symptom ?treatment ?caused_by 
                WHERE { 
                    ?disease tst:name ?name; 
                             tst:english_name ?english_name;
                             tst:description ?desc;
                             tst:caused_by ?caused_by;
                             rdf:type ?type;
                             tst:location_in ?area;
                             tst:spreading ?spreading;
                             tst:has_symptoms ?symptom;
                             tst:has_treatment ?treatment.
                    FILTER ((regex(str(?type), "%s")) && (regex(str(?area), "%s"))) .
                } """ % (tipe,area)) 

        elif(not(bentuk) and not(kondisi) and not(cuaca)): #jumlah_atribut = 9
            qres = acacia.query( 
                """ 
                PREFIX tst: <http://www.semanticweb.org/aalviian/ontologies/2017/1/untitled-ontology-10#>
                SELECT ?disease ?name ?english_name ?desc ?type ?area ?spreading ?symptom ?treatment ?caused_by 
                WHERE { 
                    ?disease tst:name ?name; 
                             tst:english_name ?english_name;
                             tst:description ?desc;
                             tst:caused_by ?caused_by; 
                             rdf:type ?type;
                             tst:location_in ?area; 
                             tst:spreading ?spreading;
                             tst:color ?color;
                             tst:has_symptoms ?symptom;
                             tst:has_treatment ?treatment.
                    FILTER ((regex(str(?type), "%s")) && (regex(str(?area), "%s")) && (regex(str(?color), "%s"))) .
                } """ % (tipe,area,warna))

        elif(not(kondisi) and not(cuaca)): #jumlah_atribut = 9
            qres = acacia.query( 
                """ 
                PREFIX tst: <http://www.semanticweb.org/aalviian/ontologies/2017/1/untitled-ontology-10#>
                SELECT ?disease ?name ?english_name ?desc ?type ?area ?spreading ?symptom ?treatment ?caused_by
                WHERE { 
                    ?disease tst:name ?name; 
                             tst:english_name ?english_name;
                             tst:description ?desc;
                             tst:caused_by ?caused_by;
                             rdf:type ?type;
                             tst:location_in ?area;
                             tst:spreading ?spreading; 
                             tst:color ?color;
                             tst:shape ?shape;
                             tst:has_symptoms ?symptom;
                             tst:has_treatment ?treatment.
                    FILTER ((regex(str(?type), "%s")) && (regex(str(?area), "%s")) && (regex(str(?color), "%s")) && (regex(str(?shape), "%s"))) .
                } """ % (tipe,area,warna,bentuk))

        elif(not(cuaca)): #jumlah_atribut = 9
            qres = acacia.query( 
                """ 
                PREFIX tst: <http://www.semanticweb.org/aalviian/ontologies/2017/1/untitled-ontology-10#>
                SELECT ?disease ?name ?english_name ?desc ?type ?area ?spreading ?symptom ?treatment ?caused_by
                WHERE { 
                    ?disease tst:name ?name;
                             tst:english_name ?english_name;
                             tst:description ?desc;
                             tst:caused_by ?caused_by;
                             rdf:type ?type;
                             tst:name ?name;
                             tst:location_in ?area; 
                             tst:spreading ?spreading;
                             tst:color ?color;
                             tst:shape ?shape;
                             tst:condition ?condition;
                             tst:has_symptoms ?symptom;
                             tst:has_treatment ?treatment.
                    FILTER ((regex(str(?type), "%s")) && (regex(str(?area), "%s")) && (regex(str(?color), "%s")) && (regex(str(?shape), "%s")) && (regex(str(?condition), "%s"))) .
                } """ % (tipe,area,warna,bentuk,kondisi))

        else: #jumlah_atribut = 9
            qres = acacia.query( 
                """ 
                PREFIX tst: <http://www.semanticweb.org/aalviian/ontologies/2017/1/untitled-ontology-10#>
                SELECT ?disease ?name ?english_name ?desc ?type ?area ?spreading ?symptom ?treatment ?caused_by
                WHERE { 
                    ?disease tst:name ?name;
                             tst:english_name ?english_name;
                             tst:description ?desc;
                             tst:caused_by ?caused_by;
                             rdf:type ?type;
                             tst:name ?name;
                             tst:location_in ?area;
                             tst:spreading ?spreading; 
                             tst:color ?color;
                             tst:shape ?shape;
                             tst:condition ?condition;
                             tst:season ?season;
                             tst:has_symptoms ?symptom;
                             tst:has_treatment ?treatment.
                    FILTER ((regex(str(?type), "%s")) && (regex(str(?area), "%s")) && (regex(str(?color), "%s")) && (regex(str(?shape), "%s")) && (regex(str(?condition), "%s")) && (regex(str(?season), "%s"))) .
                } """ % (tipe,area,warna,bentuk,kondisi,cuaca)) 

        data = []
        for row in qres:
            data.append([])
            for r in row:
                 data[-1].append(r.replace('http://www.semanticweb.org/aalviian/ontologies/2017/1/untitled-ontology-10#',''))

        penyakit=[]
        for row in data:
            cek_penyakit=1
            if penyakit==[]:
                penyakit.append([row[0],row[1],row[2],row[3],row[4],row[5],row[6],row[7],row[8],row[9]])
                continue
            
            for row2 in penyakit:
                if row[0]==row2[0]:
                    row2.append(row[9])
                    cek_penyakit=0 
                    break
                
            if cek_penyakit:
                penyakit.append([row[0],row[1],row[2],row[3],row[4],row[5],row[6],row[7],row[8],row[9]])
      
        if(not(penyakit)):
            penyakit = []
            name,english_name,desc,location,spreading = [],[],[],[],[]
            return penyakit, name, english_name, desc, location, spreading

        else:
            data = np.array(data)
            name = data[:,0]
            english_name = data[:,2]
            desc = data[:,3]
            location = data[:,5]
            spreading = data[:,6]
          
            return penyakit, name, english_name, desc, location, spreading

    def get_name_form(data):
        nama = []
        for row in data:
            nama.append(row[1])
        return nama

    def get_pathogen(data):
        patogen = []
        for row in data:
            patogen.append([])
            for row2 in range(9, len(row)):
                patogen[-1].append(row[row2])
        return patogen

    def get_location(data):
        location = []
        for row in data:
            location.append(row[5])
        return location

    def get_spreading(data):
        spreading = []
        for row in data:
            spreading.append(row[6])
        return spreading

    def get_symptom(data):
        module_dir = os.path.dirname(__file__)
        acacia = Graph().parse(os.path.join(module_dir, 'acacia.owl'))
        gejala = []
        for row in data:
            qres = acacia.query( 
                """
                PREFIX tst: <http://www.semanticweb.org/aalviian/ontologies/2017/1/untitled-ontology-10#>
                SELECT ?desc
                WHERE { 
                    ?disease tst:name ?name .
                    ?disease tst:symptom ?desc . 
                    FILTER regex(str(?disease), "%s") .
                } """ % row[7])

            for row in qres:
                for r in row:
                    gejala.append(r.replace('rdflib.term.Literal',''))

        return gejala
                
    def get_treatment(data):
        module_dir = os.path.dirname(__file__)
        acacia = Graph().parse(os.path.join(module_dir, 'acacia.owl'))
        pengendalian = []
        for row in data:
            qres = acacia.query( 
                """
                PREFIX tst: <http://www.semanticweb.org/aalviian/ontologies/2017/1/untitled-ontology-10#>
                SELECT ?desc
                WHERE { 
                    ?disease tst:name ?name .
                    ?disease tst:treatment ?desc . 
                    FILTER regex(str(?disease), "%s") .
                } """ % row[8] )

            for row in qres:
                for r in row:
                    pengendalian.append(r.replace('rdflib.term.Literal',''))
        return pengendalian

    if request.method == 'POST':
        inang = request.POST.get('inang', '')
        patogen = request.POST.get('patogen', '')
        area = request.POST.get('area', '')
        warna = request.POST.get('warna', '')
        bentuk = request.POST.get('bentuk', '')
        kondisi = request.POST.get('kondisi', '')
        cuaca = request.POST.get('cuaca', '')
        csrf_token = csrf.get_token(request)
        data, name, english_name, desc, location, spreading = get_disease_kueri_form(inang, patogen, area, warna, bentuk, kondisi,cuaca)

        if(not(data)):
            return render_to_response('index.html',{'data3' : data, 'csrf_token' : csrf_token })
        else:
            name = get_name_form(data)
            english_name = english_name
            desc = desc
            location = location
            spreading = spreading
            caused_by = get_pathogen(data)
            symptom = get_symptom(data)
            treatment = get_treatment(data)

            data = zip(name, english_name, desc, caused_by, location, spreading, symptom, treatment)
            data = list(data)

            return render_to_response('index.html',{'data3' : data, 'csrf_token' : csrf_token })
    else:
        return render_to_response('index.html')

    # except requests.exceptions.RequestException as e:
    #     print(e)
    #     sys.exit(1)