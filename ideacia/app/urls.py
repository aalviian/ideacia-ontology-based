from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^query-bm', views.querybm25, name='query-bm'),
    url(r'^query-tfidf', views.querytfidf, name='query-tfidf'),
    url(r'^query-sparql', views.query_sparql, name='query-sparql'),
    url(r'^about-us/', views.about_us, name='about-us'),
    url(r'^algorithm/', views.algorithm, name='algorithm'),
    url(r'^benchmark/', views.benchmark, name='benchmark'),
    url(r'^team/', views.team, name='team'),
]  