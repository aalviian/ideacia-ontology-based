Acacia's Disease Identification Using Semantic Ontology and Information Retrieval with BM25similarity (IDEACIA)

Environment :

1. Apache Solr 
2. Protege to create ontology and SPARQL
3. Anaconda 4.3.1 with Python 3.6
4. Sastrawi Library for stemming
4. Django Framework for Web Development