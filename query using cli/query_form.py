from rdflib import Graph
from Sastrawi.Stemmer.StemmerFactory import StemmerFactory
import numpy as np
import requests
import re
import sys
import pprint

def get_disease_kueri_form(inang, patogen, area, warna, bentuk, kondisi, cuaca):
    tipe = patogen+"_"+inang
    acacia = Graph().parse("solrowlbaru/acacia.owl")
    qres = "kosong"

    if(not(warna) and not(bentuk) and not(kondisi) and not(cuaca)): #jumlah_atribut = 9
        qres = acacia.query( 
            """ 
            PREFIX tst: <http://www.semanticweb.org/aalviian/ontologies/2017/1/untitled-ontology-10#>
            SELECT ?disease ?name ?english_name ?desc ?type ?area ?symptom ?treatment ?caused_by 
            WHERE { 
                ?disease tst:name ?name; 
                         tst:english_name ?english_name;
                         tst:description ?desc;
                         tst:caused_by ?caused_by;
                         rdf:type ?type;
                         tst:location_in ?area;
                         tst:has_symptoms ?symptom;
                         tst:has_treatment ?treatment.
                FILTER ((regex(str(?type), "%s")) && (regex(str(?area), "%s"))) .
            } """ % (tipe,area)) 

    elif(not(bentuk) and not(kondisi) and not(cuaca)): #jumlah_atribut = 9
        qres = acacia.query( 
            """ 
            PREFIX tst: <http://www.semanticweb.org/aalviian/ontologies/2017/1/untitled-ontology-10#>
            SELECT ?disease ?name ?english_name ?desc ?type ?area ?symptom ?treatment ?caused_by 
            WHERE { 
                ?disease tst:name ?name; 
                         tst:english_name ?english_name;
                         tst:description ?desc;
                         tst:caused_by ?caused_by; 
                         rdf:type ?type;
                         tst:location_in ?area; 
                         tst:color ?color;
                         tst:has_symptoms ?symptom;
                         tst:has_treatment ?treatment.
                FILTER ((regex(str(?type), "%s")) && (regex(str(?area), "%s")) && (regex(str(?color), "%s"))) .
            } """ % (tipe,area,warna))

    elif(not(kondisi) and not(cuaca)): #jumlah_atribut = 9
        qres = acacia.query( 
            """ 
            PREFIX tst: <http://www.semanticweb.org/aalviian/ontologies/2017/1/untitled-ontology-10#>
            SELECT ?disease ?name ?english_name ?desc ?type ?area ?symptom ?treatment ?caused_by
            WHERE { 
                ?disease tst:name ?name; 
                         tst:english_name ?english_name;
                         tst:description ?desc;
                         tst:caused_by ?caused_by;
                         rdf:type ?type;
                         tst:location_in ?area; 
                         tst:color ?color;
                         tst:shape ?shape;
                         tst:has_symptoms ?symptom;
                         tst:has_treatment ?treatment.
                FILTER ((regex(str(?type), "%s")) && (regex(str(?area), "%s")) && (regex(str(?color), "%s")) && (regex(str(?shape), "%s"))) .
            } """ % (tipe,area,warna,bentuk))

    elif(not(cuaca)): #jumlah_atribut = 9
        qres = acacia.query( 
            """ 
            PREFIX tst: <http://www.semanticweb.org/aalviian/ontologies/2017/1/untitled-ontology-10#>
            SELECT ?disease ?name ?english_name ?desc ?type ?area ?symptom ?treatment ?caused_by
            WHERE { 
                ?disease tst:name ?name;
                         tst:english_name ?english_name;
                         tst:description ?desc;
                         tst:caused_by ?caused_by;
                         rdf:type ?type;
                         tst:name ?name;
                         tst:location_in ?area; 
                         tst:color ?color;
                         tst:shape ?shape;
                         tst:condition ?condition;
                         tst:has_symptoms ?symptom;
                         tst:has_treatment ?treatment.
                FILTER ((regex(str(?type), "%s")) && (regex(str(?area), "%s")) && (regex(str(?color), "%s")) && (regex(str(?shape), "%s")) && (regex(str(?condition), "%s"))) .
            } """ % (tipe,area,warna,bentuk,kondisi))

    else: #jumlah_atribut = 9
        qres = acacia.query( 
            """ 
            PREFIX tst: <http://www.semanticweb.org/aalviian/ontologies/2017/1/untitled-ontology-10#>
            SELECT ?disease ?name ?english_name ?desc ?type ?area ?symptom ?treatment ?caused_by
            WHERE { 
                ?disease tst:name ?name;
                         tst:english_name ?english_name;
                         tst:description ?desc;
                         tst:caused_by ?caused_by;
                         rdf:type ?type;
                         tst:name ?name;
                         tst:location_in ?area; 
                         tst:color ?color;
                         tst:shape ?shape;
                         tst:condition ?condition;
                         tst:season ?season;
                         tst:has_symptoms ?symptom;
                         tst:has_treatment ?treatment.
                FILTER ((regex(str(?type), "%s")) && (regex(str(?area), "%s")) && (regex(str(?color), "%s")) && (regex(str(?shape), "%s")) && (regex(str(?condition), "%s")) && (regex(str(?season), "%s"))) .
            } """ % (tipe,area,warna,bentuk,kondisi,cuaca)) 

    data = []
    for row in qres:
        data.append([])
        for r in row:
             data[-1].append(r.replace('http://www.semanticweb.org/aalviian/ontologies/2017/1/untitled-ontology-10#',''))
    
    penyakit=[]
    for row in data:
        cek_penyakit=1
        if penyakit==[]:
            penyakit.append([row[0],row[1],row[2],row[3],row[4],row[5],row[6],row[7],row[8]])
            continue
        
        for row2 in penyakit:
            if row[0]==row2[0]:
                row2.append(row[8])
                cek_penyakit=0
                break
            
        if cek_penyakit:
            penyakit.append([row[0],row[1],row[2],row[3],row[4],row[5],row[6],row[7],row[8]])

          
    data = np.array(data)
    if(len(data) == 0):
        print("\nOops, pencarian tidak ditemukan\n")
        sys.exit()

    if(len(data[0]) == 9):
        name = data[:,1]
        english_name = data[:,2]
        desc = data[:,3]
        symptom = data[:,6]
        treatment = data[:,7]

    return penyakit, name, english_name, desc, symptom, treatment
    
def get_name_form(data):
    print("\nPenyakit yang ditemukan: ")
    for index, row in enumerate(data):
        print("%s. %s" % (index+1, row[1]))
 
def get_description_form(data):
    counter = 1
    for row in data:
        print("{}".format(counter), end='\n')
        print("nama : %s" % row[1])
        print("deskripsi : %s \n" % row[3])
        counter+=1

def get_pathogen_form(data):
    for index, row in enumerate(data):
        print("{}".format(index+1), end='\n')
        print("nama : %s" % row[1])
        print("patogen : ", end='')
        for c_r in range(8, len(row)):
            print(row[c_r], end='')
            if c_r == len(row)-1:
                print('.', end='')
            else:
                print(',', end=' ')
        print("\n") 

def get_symptom_form(data):
    acacia = Graph().parse("solrowlbaru/acacia.owl")
    for index, row in enumerate(data):
        print("{}.".format(index+1), end='\n')
        print("nama : %s" % row[1])
        print("gejala : %s" %row[6])
        array_qres = []
        qres = acacia.query( 
            """
            PREFIX tst: <http://www.semanticweb.org/aalviian/ontologies/2017/1/untitled-ontology-10#>
            SELECT ?desc
            WHERE { 
                ?disease tst:name ?name .
                ?disease tst:symptom_crassicarpa ?desc . 
                FILTER regex(str(?disease), "%s") .
            } """ % row[6])

        for row in qres:
            for r in row:
                print("deskripsi gejala : \n%s" % r)
            print("\n")
            
def get_treatment_form(data):
    acacia = Graph().parse("solrowlbaru/acacia.owl")
    for index, row in enumerate(data):
        print("{}.".format(index+1), end='\n')
        print("nama : %s" % row[1])
        print("pengendalian : %s" % row[7])

        array_qres = []
        qres = acacia.query( 
            """
            PREFIX tst: <http://www.semanticweb.org/aalviian/ontologies/2017/1/untitled-ontology-10#>
            SELECT ?desc
            WHERE { 
                ?disease tst:name ?name .
                ?disease tst:treatment_crassicarpa ?desc . 
                FILTER regex(str(?disease), "%s") .
            } """ % row[7])

        for row in qres:
            for r in row:
                print("deskripsi pengendalian : \n%s" % r)
            print("\n")