from rdflib import Graph
from Sastrawi.Stemmer.StemmerFactory import StemmerFactory
from query_bebas import *
from query_form import *
import numpy as np
import requests
import re
import sys
import pprint
import os

#BEBAS ONLY

def kueri_bebas():
    text = input("Masukan kueri : ")
    name_disease, description, pathogen,symptom,treatment = get_disease_kueri_bebas(text)

    return name_disease, description, pathogen, symptom, treatment

def main_menu_bebas():
    print("=============IDEACIA=============\n1. Deskripsi Penyakit \n2. Patogen Penyakit \n3. Gejala Penyakit \n4. Pengendalian Penyakit \n5. Exit Program")
    nomor = int(input("\nPilih menu: "))
    control_menu_bebas(nomor)

def control_menu_bebas(nomor):
    os.system('cls')
    welcome()
    if nomor ==  None: main_menu()
    else:
        try: list_menu_bebas[nomor]()
        except KeyError:
            print("Masukan pilihan dengan benar!\n")
            main_menu_bebas()

def menu1_bebas():
    get_description_bebas(name_disease, description)
    main_menu_bebas()

def menu2_bebas():
    get_pathogen_bebas(name_disease, pathogen)
    main_menu_bebas()

def menu3_bebas():
    get_symptom_bebas(symptom)
    main_menu_bebas()

def menu4_bebas():  
    get_treatment_bebas(treatment)
    main_menu_bebas()

list_menu_bebas = {
    1: menu1_bebas,
    2: menu2_bebas,
    3: menu3_bebas,
    4: menu4_bebas,
    5: exit,
}

#FORM ONLY

def kueri_form():
    is_inang = int(input("1. Acacia Mangium \n2. Acacia Crassicarpa \nPilih tipe Akasia (ex: 1) :"))
    if(is_inang == 1):
        inang = "mangium"
    elif(is_inang == 2):
        inang = "crassicarpa"
    else:
        print("Oops, tipe Akasia tidak ditemukan")

    is_patogen = int(input("1. Bakteri \n2. Jamur \n3. Virus \nPilih tipe patogen yang menyerang (ex: 1) :"))
    if(is_patogen == 1):
        patogen = "bacteria"
    elif(is_patogen == 2):
        patogen = "fungus"
    elif(is_patogen == 3):
        patogen = "virus"

    is_area = int(input("1. Batang \n2. Daun \n3. Akar \nPilih lokasi penyakit (ex: 1) :"))
    if(is_area == 1):
        area = "batang"
    elif(is_area == 2):
        area = "daun"
    elif(is_area == 3):
        area = "akar"

    is_warna = input("Apakah ada gejala warna bercak atau warna lainnya ? (Y/N) : ")
    if(is_warna.lower() == 'y'):
        warna = input("Masukan gejala warna bercak atau warna lainnya (jika ada, ex: hitam) :")
    else:
        warna = None

    is_bentuk = input("Apakah ada gejala bentuk bercak atau bentuk lainnya ? (Y/N) : ")
    if(is_bentuk.lower() == 'y'):
        bentuk = input("Masukan gejala bentuk bercak atau warna lainnya (jika ada, ex: memanjang) :")
    else:
        bentuk = None

    is_kondisi = input("Apakah ada gejala kondisi area penyakit ? (Y/N) : ")
    if(is_kondisi.lower() == 'y'):
        kondisi = input("Masukan gejala kondisi area penyakit (jika ada, ex: mengering) :")
    else:
        kondisi = None

    is_cuaca = input("Apakah ada gejala cuaca yang menyebabkan penyakit datang ? (Y/N) : ")
    if(is_cuaca.lower() == 'y'):
        cuaca = input("Masukan gejala cuaca (jika ada, ex: hujan lebat) :")
    else:
        cuaca = None

    data, name, english_name, desc, symptom, treatment = get_disease_kueri_form(inang, patogen, area, warna, bentuk, kondisi, cuaca)

    return data, name, english_name, desc, symptom, treatment

def main_menu_form():
    print("=============IDEACIA=============\n1. Deskripsi Penyakit \n2. Patogen Penyakit \n3. Gejala Penyakit \n4. Pengendalian Penyakit \n5. Exit Program")
    nomor = int(input("\nPilih menu: "))
    control_menu_form(nomor)

def control_menu_form(nomor):
    os.system('cls')
    welcome()
    if nomor ==  None: main_menu()
    else:
        try: list_menu_form[nomor]()
        except KeyError:
            print("Masukan pilihan dengan benar!\n")
            main_menu_form()

def menu1_form():
    get_description_form(data)
    main_menu_form()

def menu2_form():
    get_pathogen_form(data)
    main_menu_form()

def menu3_form():
    get_symptom_form(data)
    main_menu_form()

def menu4_form():
    get_treatment_form(data)
    main_menu_form()

def exit():
    os.system('cls')
    sys.exit()

def welcome():
    print("========================================================================================")
    print("Sistem Identifikasi Penyakit Akasia Menggunakan Semantic Ontologi dan TKI BM25Similarity")
    print("Bekerja Sama Dengan PT.Arara Abadia, Riau")
    print("========================================================================================")

list_menu_form = {
    1: menu1_form,
    2: menu2_form,
    3: menu3_form,
    4: menu4_form,
    5: exit,
}

if __name__ == "__main__":
    welcome()
    jenis = int(input("1.Live Query \n2.List Query \n3.Exit Program \nPilih jenis kueri (ex: 1):"))
    if(jenis == 1): 
        name_disease, pathogen, description, symptom, treatment = kueri_bebas()
        get_name_bebas(name_disease)
        print("\n")
        main_menu_bebas()

    elif(jenis == 2): 
        data, name, english_name, description, symptom, treatment  = kueri_form()
        get_name_form(data)
        main_menu_form()

    else:
        print("Have a nice day!")
        sys.exit()
