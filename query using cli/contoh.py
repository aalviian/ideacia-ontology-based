import numpy as np
import requests
import sys
import pprint
import os
from rdflib import Graph
import re

module_dir = os.path.dirname(__file__)
acacia = Graph().parse(os.path.join(module_dir, 'acacia.owl'))
patogen = []
id_disease = 'penyakit_tepung'
qres = acacia.query( 
    """
    PREFIX tst: <http://www.semanticweb.org/aalviian/ontologies/2017/1/untitled-ontology-10#>
    SELECT ?patogen
    WHERE { 
        ?disease tst:caused_by ?patogen . 
        FILTER regex(str(?disease), "%s") .
    } """ % id_disease )

    for row in qres:
        for r in row:
            patogen.append(r.replace('http://www.semanticweb.org/aalviian/ontologies/2017/1/untitled-ontology-10#',''))

print(patogen)