import requests

class Caused:
    id = ""
    name = ""

class Location:
    id = ""
    name = ""

class Disease:
    id = ""
    type = ""
    description = ""
    caused_by = Caused()

class DiseaseOntology:
    
    def GetDiseaseAll(self, type):
        solr_url = "http://localhost:8983/solr/ontology/select"
        url = solr_url + "?q=" + type + "&start=0&rows=100000&wt=json"

        response = requests.get(url)

        result = response.json()

        if result == None:
            return None

        result_response = None
        if "response" in result:
            result_response = result["response"]

        if result_response == None:
            return None

        query_result = []
        if len(result_response["docs"]) > 0:
            for doc in result_response["docs"]:
                doc_result = self.BindJsonToDisease(doc)
                query_result.append(doc_result)
                
        return query_result

    def GetCausedBy(self, id):
        solr_url = "http://localhost:8983/solr/ontology/select"
        url = solr_url + "?q=id:" + id + "&start=0&rows=100000&wt=json"
        response = requests.get(url)

        result = response.json()

        if result == None:
            return None

        result_response = None
        if "response" in result:
            result_response = result["response"]

        if result_response == None:
            return None

        caused_by = None
        if len(result_response["docs"]) > 0:
            for doc in result_response["docs"]:
                print(doc)
                caused_by = self.BindJsonToCause(doc)
                print(caused_by)
            
        return caused_by
        
    def BindJsonToCause(self, doc):
        caused_by = Caused()

        caused_by.id = doc["id"]
        if "name" in doc:
            caused_by.name = doc["name"]

        return caused_by
    
    def BindJsonToDisease(self, doc):
        disease = Disease()

        if "id" in doc:
            disease.id = doc["id"][0]

        if "type" in doc:
            disease.type = doc["type"][0]

        description = ""
        if "description" in doc:
            disease.description = doc["description"][0]

        # caused_by_id = ""
        # if "caused_by" in doc:
        #     caused_by_id = doc["caused_by"][0]

        # print(caused_by_id)
        
        # caused_by = self.GetCausedBy(caused_by_id)
        # disease.caused_by = caused_by
        
        return disease

onto = DiseaseOntology()
print(onto.GetDiseaseAll('bercak'))