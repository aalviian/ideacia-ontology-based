import xml.etree.ElementTree as ET
from bs4 import BeautifulSoup
from time import sleep
import sys

root = ET.parse('acacia.owl').getroot()

file_root = ET.Element("add") 
 
for child in root:
    id = ''
    try:
        id = child.attrib['{http://www.w3.org/1999/02/22-rdf-syntax-ns#}about']
        id = id.replace('http://www.semanticweb.org/aalviian/ontologies/2017/1/untitled-ontology-10#', '')
        id = id.replace('http://swrl.stanford.edu/ontologies/3.3/swrla.owl#', '')
    except:
        id = ''

    if id != '':
        file_doc = ET.SubElement(file_root, "doc")
        ET.SubElement(file_doc, "field", name="id").text = id

    for item in child:
        tag = item.tag
        tag = tag.replace('{http://www.semanticweb.org/aalviian/ontologies/2017/1/untitled-ontology-10#}', '')
        tag = tag.replace('{http://www.w3.org/1999/02/22-rdf-syntax-ns#}', '')
        tag = tag.replace('{http://www.w3.org/2000/01/rdf-schema#}', '')
        tag = tag.replace('http://www.w3.org/2002/07/owl#', '')
        text = ''
        try:
            text = item.attrib['{http://www.w3.org/1999/02/22-rdf-syntax-ns#}resource']
            if text is not None:
                text = text.replace('http://www.semanticweb.org/aalviian/ontologies/2017/1/untitled-ontology-10#', '')
                text = text.replace('http://www.w3.org/2002/07/owl#', '')
        except:
            text = item.text
            if text is not None:
                text = text.replace('http://www.w3.org/2002/07/owl#', '')
                text = text.replace('http://www.w3.org/2000/01/rdf-schema#', '')

        ET.SubElement(file_doc, "field", name=tag).text = text

tree = ET.ElementTree(file_root)
tree.write("disease_onto.xml", xml_declaration=True, encoding='utf-8')
ask = input("Do you want to pretttify disease_onto.xml ? (Y/N) : ")
if (ask == 'Y' or ask == 'y'):
	sleep(3)
	bs = BeautifulSoup(open('disease_onto.xml'), 'xml')
	f = open('disease_onto.xml', "w")
	f.write(bs.prettify())
	f.close()
	print("Successfully create a beautiful XML => disease_onto.xml")
else:
	print("Successfully create a dirty XML => disease_onto.xml")
	sys.exit()