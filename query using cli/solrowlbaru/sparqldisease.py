from rdflib import Graph
import re

poGraph = Graph().parse("acacia.owl")
keyword = 'stage'
#poGraph = Graph().parse("http://purl.obolibrary.org/obo/po.owl", format='xml')
qres = poGraph.query( 
    """ PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX owl: <http://www.w3.org/2002/07/owl#>
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
    PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
    PREFIX tst: <http://www.semanticweb.org/aalviian/ontologies/2017/1/untitled-ontology-10#>
    SELECT ?desc
    WHERE { 
        ?name tst:symptom_crassicarpa ?desc . 
        FILTER regex(str(?name), "busuk_batang_gejala") .
    }

"""
    )

for row in qres:
    #p = re.compile('_crassicarpa')
    #if(p.match(hasil)):
    #    print("Lokasi di => {} ".format(hasil))l
    # else:
    #     print("Penyakit => {} ".format(hasil))
    print(row[0])
    print("\n")