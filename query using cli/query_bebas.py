from rdflib import Graph
from Sastrawi.Stemmer.StemmerFactory import StemmerFactory
import numpy as np
import requests
import re
import sys
import pprint

def stemming_word(text):
    words = text.split()
    array_query = []
    for row in words:
        array_query.append(row.strip())
    print("Kueri => {} \n".format(array_query))

    query = ' '.join(map(str, array_query))
    factory = StemmerFactory()
    stemmer = factory.create_stemmer()
    query   = stemmer.stem(query)
    print("Hasil stemming => {}".format(query))
    return query 

def remove_stopword(query):
    fileName = 'stopword.txt'
    with open(fileName) as f:
        data = f.readlines()

    stopwords = []
    array_query = []
    for row in data:
        stopwords.append(row.strip())

    query = query.split()
    for row in query:
        array_query.append(row)

    for query in array_query:
        if(query in stopwords):
            array_query.remove(query)

    query = ' '.join(map(str, array_query))

    print("Hasil pembuangan stopwords => {}".format(query))
    return query

def get_disease_kueri_bebas(query):
    query = stemming_word(query)
    query = remove_stopword(query)
    url = 'http://localhost:8983/solr/onto-bm25/select?q=' + query + '&wt=json&indent=true&debugQuery=true'
    try:
        result = requests.get(url)
        result = result.json()
        if result != None:
            result_response = None
            if "response" in result:
                result_response = result["response"] 
                count = len(result_response['docs'])

        name = []
        pathogen = []
        description = []
        symptom = []
        treatment = []
        for result in result_response['docs']:
            if('gejala' in result['id']): continue
            elif('pengendalian' in result['id']): continue
            else:
                if('caused_by' in result):
                    name.append(result['name'][0].strip())
                    pathogen.append([])
                    description.append(result['description'][0].strip())
                    for row in result['caused_by']:
                        pathogen[-1].append(row.strip())
                    symptom.append(result['has_symptoms'][0].strip())
                    treatment.append(result['has_treatment'][0].strip())
                else:
                   continue

        if(len(name) == 0):
            print("\nOops, pencarian tidak ditemukan\n")
            sys.exit()

        return name, pathogen, description, symptom, treatment
        
    except requests.exceptions.RequestException as e:
        print(e)
        sys.exit(1)

def get_name_bebas(name):
    print("\nPenyakit yang ditemukan: ")
    for index, name in enumerate(name):
        print("{}. {}".format(index+1, name))
        
def get_description_bebas(name, description):
    data = zip(name, description)
    counter = 1
    for nama, desc in data:
        print("{}.".format(counter), end='\n')
        print("nama : %s" % nama)
        print("description : %s \n" % desc)
        counter+=1

def get_pathogen_bebas(name, pathogen):
    data = zip(name, pathogen)
    counter = 1
    char = 'a'
    for nama, patogen in data:
        print("{}.".format(counter), end='\n')
        print("nama : %s" % nama)
        print("patogen : ")
        for patogen in patogen:
            print("{}. {}".format(char, patogen))
            char = chr(ord(char)+1)
        print("\n")
        counter+=1
        char = 'a'

def get_symptom_bebas(symptom):
    for index, symptom in enumerate(symptom):
        url = 'http://localhost:8983/solr/onto-bm25/select?q=id:*'+symptom+'*&wt=json'
        try:
            result = requests.get(url)
            result = result.json()
            if result != None:
                result_response = None
                if "response" in result: 
                    result_response = result["response"] 
                    count = len(result_response['docs'])

            for result in result_response['docs']:
                if('gejala' in result['id']):
                    print("{}. {}\n".format(index+1, result['name'][0].strip()))
                    print(result['symptom'][0].strip(), end='\n')
                    print("\n")
                else: continue
        
        except requests.exceptions.RequestException as e:
            print(e)
            sys.exit(1)

def get_treatment_bebas(treatment):
    for index, treatment in enumerate(treatment):
        url = 'http://localhost:8983/solr/onto-bm25/select?q=id:*'+treatment+'*&wt=json'
        try:
            result = requests.get(url)
            result = result.json()
            if result != None:
                result_response = None
                if "response" in result: 
                    result_response = result["response"] 
                    count = len(result_response['docs'])

            for result in result_response['docs']:
                if('pengendalian' in result['id']):
                    print("{}. {}\n".format(index+1, result['name'][0].strip()))
                    print(result['treatment'][0].strip(), end='\n')
                    print("\n")
                else: continue
        
        except requests.exceptions.RequestException as e:
            print(e)
            sys.exit(1)